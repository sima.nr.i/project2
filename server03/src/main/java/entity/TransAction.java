package entity;

import java.io.Serializable;

/**
 * Created by Hi on 7/27/2020.
 */
public class TransAction implements Serializable {
    private static final long serialVersionUID = 6529685098267757690L;

    private long id, amount, deposit;
    private String type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public long getDeposit() {
        return deposit;
    }

    public void setDeposit(long deposit) {
        this.deposit = deposit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
