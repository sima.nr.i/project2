package entity;

import fileReader.CoreJsonReader;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Hi on 7/27/2020.
 */
public class TransActionImp {
    private static final long serialVersionUID = 6529685098267757690L;

    public TransActionImp() throws RemoteException {
    }

    public synchronized List<Response> transAction(List<TransAction> transActions) {
        List<Response> responses = new ArrayList<Response>();
        try {
            List<Corejson> corejsonList = new ArrayList<Corejson>();
            CoreJsonReader coreJsonReader = new CoreJsonReader();
            corejsonList = coreJsonReader.parseJsonObject();
            String log = "";
            //log
            for (Corejson corejson : corejsonList) {
                log = corejson.getOutLog();

            }
            Logger logger = Logger.getLogger("MyLog");
            FileHandler fh;
            // This block configure the logger with handler and formatter
            //C:\Users\Hi\IdeaProjects\server.d\src\resourses\log.log
            fh = new FileHandler("C:/Users/Hi/IdeaProjects/server03/src/main/java/resource" + log + ".log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            responses = new ArrayList<Response>();
            long initialBalance = 0;
            long upperBound = 0;
            for (TransAction transAction : transActions) {
                Response response = new Response();
                //find customerName
                for (Corejson corejson : corejsonList) {
                    List<Deposit> depositList = new ArrayList<Deposit>();
                    depositList = corejson.getDeposits();
                    for (Deposit deposit : depositList) {
                        if (Long.parseLong(deposit.getId()) == transAction.getDeposit()) {
                            response.setCustomerName(deposit.getCustomer());
                            initialBalance = (Long.parseLong(deposit.getInitialBalance()));
                            upperBound = (Long.parseLong(deposit.getUpperBound()));
                        }
                    }
                }

                if (transAction.getType().equals("deposit")) {
                    if (initialBalance + transAction.getAmount() < upperBound) {
                        initialBalance = transAction.getAmount() + initialBalance;
                        response.setBalance(initialBalance);
                        response.setRscode("0");
                        logger.info("Deposit Done");
                    } else {
                        try {
                            response.setRscode("1");
                            logger.info("The Operation Of Deposit Faild ");
                            logger.info("mizan mojodi az saghf balatar ast");
                            throw new Exception("mizan mojodi az saghf balatar ast");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } else {
                    if (initialBalance - transAction.getAmount() > upperBound) {
                        initialBalance = initialBalance - transAction.getAmount();
                        response.setBalance(initialBalance);
                        response.setRscode("0");
                        logger.info("withdraw done");
                    } else {
                        try {
                            response.setRscode("1");
                            logger.info("The Operation Of withdraw Faild ");
                            logger.info("mizan mojodi az kaf kamtar ast");
                            throw new Exception("mizan mojodi az kaf kamtar ast");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                responses.add(response);
                System.out.println("response ok");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responses;
    }

}
