package common;

import entity.Response;
import entity.TransAction;
import entity.TransActionImp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hi on 9/27/2020.
 */
public class Server {
    private static final long serialVersionUID = 6529685098267757690L;
//    static final long serialVersionUID = 42L;
        private ServerSocket server;
        private Socket socket;
        private int port;
       // private DataInputStream in   = null;
 // private DataOutputStream out= null;
        public Server(int port) throws IOException, ClassNotFoundException {
        server = new ServerSocket(port);
        System.out.println("Server started");
        System.out.println("Waiting for a client ...");
        socket = server.accept();
        System.out.println("Client accepted");
        // takes input from the client socket
       // in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        ObjectInputStream objectInputStream=new ObjectInputStream(socket.getInputStream());
        ObjectOutputStream objectOutputStream=new ObjectOutputStream(socket.getOutputStream());
       // Terminal terminal= (Terminal) objectInputStream.readObject();
            System.out.println("before read object");
            Object inputObject=objectInputStream.readObject();
         List<TransAction>transActions=new ArrayList<TransAction>();
         transActions=(List<TransAction>)inputObject;
       //List<TransAction> transActions= (List<TransAction>) objectInputStream.readObject();
            System.out.println("after read ");
        TransActionImp transActionImp=new TransActionImp();
        List<Response> responses=transActionImp.transAction(transActions);
            System.out.println("" +
                    "" +
                    "response");
        objectOutputStream.writeObject(responses);
    }
}
