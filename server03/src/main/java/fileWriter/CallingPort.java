package fileWriter;

import entity.Corejson;
import fileReader.CoreJsonReader;

import java.util.List;

/**
 * Created by Hi on 7/27/2020.
 */
public class CallingPort {
    private static final long serialVersionUID = 6529685098267757690L;

    public String callPort() {
        String port = "";
        try {
            CoreJsonReader coreJsonReader = new CoreJsonReader();
            List<Corejson> corejsonList = coreJsonReader.parseJsonObject();
            for (Corejson corejson : corejsonList) {
                port = corejson.getPort();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return port;
    }
}
