package fileReader;

import entity.TransAction;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Hi on 7/22/2020.
 */
public class XMLParser {
    private static final long serialVersionUID = 6529685098267757690L;

    public List<TransAction> xmlParser() {
        List<TransAction> transActions = new ArrayList<TransAction>();
//        Terminal terminal=new Terminal();
        try {
            File inputFile = new File("src/main/java/resource/Terminal.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = null;
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = null;
            doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            Logger logger = Logger.getLogger("MyLog");
            FileHandler fh;
            fh = new FileHandler("C:/Users/Hi/IdeaProjects/client03/src/main/java/resource/MyLogFile.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
//            NodeList nodeListTerminal=doc.getElementsByTagName("server");
//            for (int temp1 = 0; temp1 < nodeListTerminal.getLength(); temp1++) {
//                TransAction transAction = new TransAction();
//                Node nNode = nodeListTerminal.item(temp1);
//                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//                    Element eElement = (Element) nNode;
//                    String ip = eElement.getAttribute("ip");
//                    String port = eElement.getAttribute("port");
//                    logger.info("Entering ip & port");
//                    if (ip.equals("")) {
//                        try {
//                            logger.info("ip is null");
//                            throw new Exception("ip ra vard konid");
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    else {
//                        terminal.setIp(ip);
//                        mainIp=ip;
//                    }
//                    if (port.equals("")) {
//                        try {
//                            logger.info("port is null");
//                            throw new Exception("port ra vard konid");
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    else {terminal.setPort(port);
//                    mainPort=port;
//                    }
//                }
//            }
            NodeList nList = doc.getElementsByTagName("transaction");
            transActions = new ArrayList<TransAction>();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                TransAction transAction = new TransAction();
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String id = eElement.getAttribute("id");
                    String type = eElement.getAttribute("type");
                    String amount = eElement.getAttribute("amount");
                    String deposit = eElement.getAttribute("deposit");
                    logger.info("Entering inputs");
                    //customerNumber
                    if (id.equals("")) {
                        try {
                            logger.info("id is null");
                            throw new Exception("id ra vard konid");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        transAction.setId(Long.parseLong(id));
                    if (type.equals("")) {
                        try {
                            logger.info("type is null");
                            throw new Exception("type ra vard konid");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        transAction.setType(type);
                    if (amount.equals("")) {
                        try {
                            logger.info("amount is null");
                            throw new Exception("amount ra vard konid");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        transAction.setAmount(Long.parseLong(amount));
                    if (deposit.equals("")) {
                        try {
                            logger.info("deposite is null");
                            throw new Exception("deposite ra vard konid");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        transAction.setDeposit(Long.parseLong(deposit));
                }
                transActions.add(transAction);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return transActions;
    }
}