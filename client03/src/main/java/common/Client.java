package common;

import entity.Response;
import entity.TransAction;
import fileReader.XMLParser;
import fileWritter.Result;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hi on 9/26/2020.
 */
public class Client {
    private static final long serialVersionUID = 6529685098267757690L;
    private Socket socket = null;
    private DataInputStream input = null;
    private DataOutputStream out = null;

    public Client(String address, int port) {
        try {
            socket = new Socket(address, port);
            System.out.println("Connected");
            // input = new DataInputStream(System.in);
            //out = new DataOutputStream(socket.getOutputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            XMLParser xmlParser = new XMLParser();
            List<TransAction> transActions = new ArrayList<TransAction>();
//            terminal[0] =xmlParser.xmlParser();
//            objectOutputStream.writeObject(terminal[0]);
            //XMLParser xmlParser=new XMLParser();
            //List<TransAction> transActions=new ArrayList<TransAction>();
            transActions = xmlParser.xmlParser();
            objectOutputStream.writeObject(transActions);

//                    Thread thread1 = new Thread() {
//            @Override
//            public void run() {
//                try {
//
//                    XMLParser xmlParser=new XMLParser();
//                    List<TransAction> transActions=new ArrayList<TransAction>();
//                    transActions =xmlParser.xmlParser();
//                    objectOutputStream.writeObject(transActions);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//        Thread thread2 = new Thread() {
//            @Override
//            public void run() {
//                try {
//                    XMLParser xmlParser=new XMLParser();
//                    List<TransAction> transActions=new ArrayList<TransAction>();
//                    transActions =xmlParser.xmlParser();
//                    objectOutputStream.writeObject(transActions);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//        thread1.start();
//        thread2.start();
            List<Response> responses = (List<Response>) objectInputStream.readObject();
            System.out.println("get response");
            Result result = new Result();
            System.out.println("object result");
            result.result(responses);
            System.out.println("do result method");
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
