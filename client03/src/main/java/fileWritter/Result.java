package fileWritter;

import entity.Response;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

/**
 * Created by Hi on 7/27/2020.
 */
public class Result {
    private static final long serialVersionUID = 6529685098267757690L;
    public static final String xmlFilePath = "C:\\Users\\Hi\\IdeaProjects\\client03\\src\\main\\java\\resource\\response1.xml";

    public void result(List<Response> responseList) throws Exception {
        //XMLParser xmlParser = new XMLParser();
        //List<TransAction> transActions = new ArrayList<TransAction>();
        ///Terminal terminal = xmlParser.xmlParser();
        // final List<TransAction> finalTransActions = transActions;
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();
        // root element
        Element root = document.createElement("response");
        document.appendChild(root);
        for (Response response : responseList) {
            System.out.println(response.getCustomerName() + "      balance:" + response.getBalance() + "     rscode:" + response.getRscode());
            //response element
            Element seporde = document.createElement("seporde");
            root.appendChild(seporde);
            // name element
            Element customerName = document.createElement("customerName");
            customerName.appendChild(document.createTextNode(response.getCustomerName()));
            seporde.appendChild(customerName);
            //balanceElement
            Element balance = document.createElement("balance");
            balance.appendChild(document.createTextNode(String.valueOf(response.getBalance())));
            seporde.appendChild(balance);
            //rscode
            Element rscode = document.createElement("rscode");
            rscode.appendChild(document.createTextNode(String.valueOf(response.getRscode())));
            seporde.appendChild(rscode);
        }
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File(xmlFilePath));
        transformer.transform(domSource, streamResult);
        System.out.println("Done creating XML File");


    }
}
